from django.db import models


# Create your models here.

class Department(models.Model):
    title = models.CharField(verbose_name="标题", max_length=32)
    count = models.IntegerField(verbose_name="人数")


class UserInfo(models.Model):
    username = models.CharField(verbose_name='用户名', max_length=32)
    password = models.CharField(verbose_name='密码', max_length=64)
    age = models.IntegerField(verbose_name='年龄')
    mobile = models.CharField(verbose_name='手机号', max_length=11)


class User(models.Model):
    name = models.CharField(verbose_name='姓名', max_length=12)
    age = models.IntegerField(verbose_name='年龄')
    salary = models.IntegerField(verbose_name='工资')
    depart = models.ForeignKey(verbose_name='关联部门', to='Department', on_delete=models.CASCADE)
