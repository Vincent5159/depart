from django import forms
from django.shortcuts import render, redirect

from app01 import models


def depart(request):
    # models.Department.objects.create(title='销售部', count=10)
    # models.Department.objects.create(**{'title': '技术部', 'count': 5})
    # return HttpResponse('success')

    # queryset = models.Department.objects.all()
    # queryset = models.Department.objects.filter(id__gt=0)    #id>0
    # for obj in queryset:
    #     print(obj.id, obj.title, obj.count)
    queryset = models.Department.objects.all().order_by('id')  # 基于id排序  -id  为倒序
    return render(request, 'depart.html', {'queryset': queryset})


def depart_add(request):
    if request.method == 'GET':
        return render(request, 'depart_add.html')
    title = request.POST.get('title')
    count = request.POST.get('count')
    models.Department.objects.create(title=title, count=count)
    return redirect('/depart/')  # 跳转回列表页面


def depart_delete(request):
    depart_id = request.GET.get('id')
    models.Department.objects.filter(id=depart_id).delete()
    return redirect('/depart/')


def depart_edit(request):
    if request.method == 'GET':  # 从列表页面跳转过来的时候
        depart_id = request.GET.get('id')
        depart_object = models.Department.objects.filter(id=depart_id).first()
        return render(request, 'depart_edit.html', {'depart_object': depart_object})
    depart_id = request.GET.get('id')  # 编辑提交数据
    title = request.POST.get('title')
    count = request.POST.get('count')
    models.Department.objects.filter(id=depart_id).update(title=title, count=count)
    return redirect('/depart/')


def login(request):
    """用户登录"""
    if request.method == 'GET':
        return render(request, 'login.html')

    user = request.POST.get('user')
    pwd = request.POST.get('pwd')
    # 数据库校验  如果成功 1.生成随机字符串 2.返回到用户浏览器的cookie中 3.存储到网站的session中 随机字符串+用户标识
    #           如果失败，展示错误信息
    user_object = models.UserInfo.objects.filter(username=user, password=pwd).first()
    if user_object:
        request.session['info'] = {'name': user_object.username, 'id': user_object.id}
        return redirect('/home/')
    else:
        return render(request, 'login.html', {'error': "username or password error !"})


def home(request):
    info_dict = request.info_dict

    print('当前登录用户:', info_dict)
    return render(request, 'home.html', {'info_dict': info_dict})


def user_list(request):
    queryset = models.User.objects.all()
    return render(request, 'user_list.html', {"queryset": queryset})


def add_user(request):
    if request.method == 'GET':
        depart_list = models.Department.objects.all()
        return render(request, 'add_user.html', {"depart_list": depart_list})

    user = request.POST.get('user')
    age = request.POST.get('age')
    salary = request.POST.get('salary')
    dp = request.POST.get('dp')
    models.User.objects.create(name=user, age=age, salary=salary, depart_id=dp)

    return redirect('/user/list/')


class RoleForm(forms.Form):
    user = forms.CharField(label='用户名')
    password = forms.CharField(label='密码')
    email = forms.CharField(label='邮箱')


def add_role(request):
    form = RoleForm()
    return render(request, 'add_role.html', {'form': form})

