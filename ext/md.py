from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect


class AuthMiddleware(MiddlewareMixin):
    def process_request(self, request):

        # 如果是登录页面直接向后运行
        if request.path_info == '/login/':
            return

        # 除了/login/以外的其他页面都需要做验证
        info_dict = request.session.get('info')
        if info_dict:
            request.info_dict = info_dict   # 给request中赋值
            return

        return redirect('/login/')
